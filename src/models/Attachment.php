<?php namespace Whitegolem\Filehandler;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model {

	protected $guarded = array('id');

	protected $fillable = array(
		'name',
	);

	protected static $rules = array(
		'name' => 'required'
	);

	/**
	* Relationships
	*/
	public function attachable()
	{
		return $this->morphTo();
	}

}