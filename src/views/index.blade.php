<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>jQuery File Upload</title>
</head>
<body>

	<div class="container">
		<form id="fileupload" action="#" method="POST" enctype="multipart/form-data">
			<!-- Redirect browsers with JavaScript disabled to the origin page -->
			<noscript><input type="hidden" name="redirect" value="http://blueimp.github.com/jQuery-File-Upload/"></noscript>
			<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
			<div class="row-fluid fileupload-buttonbar">
				<div class="span7">
					<!-- The fileinput-button span is used to style the file input field as button -->
					<span class="btn btn-success fileinput-button">
						<i class="icon-plus icon-white"></i>
						<span>Add files...</span>
						<input type="file" name="files[]" multiple>
					</span>
					<button type="submit" class="btn btn-primary start">
						<i class="icon-upload icon-white"></i>
						<span>Start upload</span>
					</button>
					<button type="reset" class="btn btn-warning cancel">
						<i class="icon-ban-circle icon-white"></i>
						<span>Cancel upload</span>
					</button>
					<button type="button" class="btn btn-danger delete">
						<i class="icon-trash icon-white"></i>
						<span>Delete</span>
					</button>
					<input type="checkbox" class="toggle">
				</div>
				<!-- The global progress information -->
				<div class="span5 fileupload-progress fade">
					<!-- The global progress bar -->
					<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
						<div class="bar" style="width:0%;"></div>
					</div>
					<!-- The extended global progress information -->
					<div class="progress-extended">&nbsp;</div>
				</div>
			</div>
			<!-- The loading indicator is shown during file processing -->
			<div class="fileupload-loading"></div>
			<br>
			<!-- The table listing the files available for upload/download -->
			<table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
		</form>
	</div><!--/container-->

	<!-- modal-gallery is the modal dialog used for the image gallery -->
	<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3 class="modal-title"></h3>
		</div>
		<div class="modal-body"><div class="modal-image"></div></div>
		<div class="modal-footer">
			<a class="btn modal-download" target="_blank">
				<i class="icon-download"></i>
				<span>Download</span>
			</a>
			<a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
				<i class="icon-play icon-white"></i>
				<span>Slideshow</span>
			</a>
			<a class="btn btn-info modal-prev">
				<i class="icon-arrow-left icon-white"></i>
				<span>Previous</span>
			</a>
			<a class="btn btn-primary modal-next">
				<span>Next</span>
				<i class="icon-arrow-right icon-white"></i>
			</a>
		</div>
	</div>

	<!-- The template to display files available for upload -->
	<script id="template-upload" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-upload fade">
			<td>
				<span class="preview"></span>
			</td>
			<td>
				<p class="name">{%=file.name%}</p>
				{% if (file.error) { %}
					<div><span class="label label-important">Error</span> {%=file.error%}</div>
				{% } %}
			</td>
			<td>
				<p class="size">{%=o.formatFileSize(file.size)%}</p>
				{% if (!o.files.error) { %}
					<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
				{% } %}
			</td>
			<td>
				{% if (!o.files.error && !i && !o.options.autoUpload) { %}
					<button class="btn btn-primary start">
						<i class="icon-upload icon-white"></i>
						<span>Start</span>
					</button>
				{% } %}
				{% if (!i) { %}
					<button class="btn btn-warning cancel">
						<i class="icon-ban-circle icon-white"></i>
						<span>Cancel</span>
					</button>
				{% } %}
			</td>
		</tr>
	{% } %}
	</script>

	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-download fade">
			<td>
				<span class="preview">
					{% if (file.thumbnail_url) { %}
						<a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
					{% } %}
				</span>
			</td>
			<td>
				<p class="name">
					<a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
				</p>
				{% if (file.error) { %}
					<div><span class="label label-important">Error</span> {%=file.error%}</div>
				{% } %}
			</td>
			<td>
				<span class="size">{%=o.formatFileSize(file.size)%}</span>
			</td>
			<td>
				<button class="btn btn-danger delete" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
					<i class="icon-trash icon-white"></i>
					<span>Delete</span>
				</button>
				<input type="checkbox" name="delete" value="1" class="toggle">
			</td>
		</tr>
	{% } %}
	</script>

	<!-- Bootstrap CSS Toolkit styles -->
	<link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap.min.css">
	<!-- Generic page styles -->
	<link rel="stylesheet" href="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/css/style.css') }}">
	<!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
	<link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-responsive.min.css">
	<!-- Bootstrap CSS fixes for IE6 -->
	<!--[if lt IE 7]><link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-ie6.min.css"><![endif]-->
	<!-- Bootstrap Image Gallery styles -->
	<link rel="stylesheet" href="http://blueimp.github.com/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/css/jquery.fileupload-ui.css') }}">
	<!-- CSS adjustments for browsers with JavaScript disabled -->
	<noscript><link rel="stylesheet" href="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/css/jquery.fileupload-ui-noscript.css') }}"></noscript>



	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/vendor/jquery.ui.widget.js') }}"></script>
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/blueimp/tmpl.min.js') }}"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/blueimp/load-image.min.js') }}"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/blueimp/canvas-to-blob.min.js') }}"></script>
	<!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
	<script src="http://blueimp.github.com/cdn/js/bootstrap.min.js"></script>
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.iframe-transport.js') }}"></script>
	<!-- The basic File Upload plugin -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.fileupload.js') }}"></script>
	<!-- The File Upload processing plugin -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.fileupload-process.js') }}"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.fileupload-image.js') }}"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.fileupload-audio.js') }}"></script>
	<!-- The File Upload video preview plugin -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.fileupload-video.js') }}"></script>
	<!-- The File Upload validation plugin -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.fileupload-validate.js') }}"></script>
	<!-- The File Upload user interface plugin -->
	<script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/jquery.fileupload-ui.js') }}"></script>
	<!-- The main application script -->
	<!--script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/main.js') }}"></script-->
	<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
	<!--[if gte IE 8]><script src="{{ URL::asset('packages/whitegolem/filehandler/js/jQuery-File-Upload/js/cors/jquery.xdr-transport.js') }}"></script><![endif]-->


	<script>

		(function($){
			
			$(function(){
				'use strict';

				// Initialize the jQuery File Upload widget:
				$('#fileupload').fileupload({
					// Uncomment the following to send cross-domain cookies:
					//xhrFields: {withCredentials: true},
					url: '{{ URL::route('filehandler.files') }}',
					maxChunkSize: 10000000, //10000000
				});

				// Enable iframe cross-domain access via redirect option:
				$('#fileupload').fileupload(
					'option',
					'redirect',
					window.location.href.replace(
						/\/[^\/]*$/,
						'/cors/result.html?%s'
					)
				);


				// Load existing files:
				$('#fileupload').addClass('fileupload-processing');
				$.ajax({
					// Uncomment the following to send cross-domain cookies:
					//xhrFields: {withCredentials: true},
					url: $('#fileupload').fileupload('option', 'url'),
					dataType: 'json',
					context: $('#fileupload')[0]
				}).always(function (result) {
					$(this).removeClass('fileupload-processing');
				}).done(function (result) {
					$(this).fileupload('option', 'done')
						.call(this, null, {result: result});
				});


				// DEBUG
				//================================================================================================
				// $('#fileupload')
				// .on('fileuploadadd', function (e, data) {console.log('fileuploadadd',e, data);})
				// .on('fileuploadsubmit', function (e, data) {console.log('fileuploadsubmit',e, data);})
				// .on('fileuploadsend', function (e, data) {console.log('fileuploadsend',e, data);})
				// .on('fileuploaddone', function (e, data) {console.log('fileuploaddone',e, data);})
				// .on('fileuploadfail', function (e, data) {console.log('fileuploadfail',e, data);})
				// .on('fileuploadalways', function (e, data) {console.log('fileuploadalways',e, data);})
				// .on('fileuploadprogress', function (e, data) {console.log('fileuploadprogress',e, data);})
				// .on('fileuploadprogressall', function (e, data) {console.log('fileuploadprogressall',e, data);})
				// .on('fileuploadstart', function (e) {console.log('fileuploadstart',e);})
				// .on('fileuploadstop', function (e) {console.log('fileuploadstop',e);})
				// .on('fileuploadchange', function (e, data) {console.log('fileuploadchange',e, data);})
				// .on('fileuploadpaste', function (e, data) {console.log('fileuploadpaste',e, data);})
				// .on('fileuploaddrop', function (e, data) {console.log('fileuploaddrop',e, data);})
				// .on('fileuploaddragover', function (e) {console.log('fileuploaddragover',e);})
				// .on('fileuploadchunksend', function (e, data) {console.log('fileuploadchunksend',e, data);})
				// .on('fileuploadchunkdone', function (e, data) {console.log('fileuploadchunkdone',e, data);})
				// .on('fileuploadchunkfail', function (e, data) {console.log('fileuploadchunkfail',e, data);})
				// .on('fileuploadchunkalways', function (e, data) {console.log('fileuploadchunkalways',e, data);});

				
			});

		})(jQuery);
	</script>
	
</body>
</html>