<?php namespace Whitegolem\Filehandler;

// use Illuminate\Support\Str;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;

use Intervention\Image\Image;

include __DIR__.'/../../../public/js/jQuery-File-Upload-8.9.0/server/php/UploadHandler.php';

class FileHandler extends \UploadHandler{

	use Traits\Previewable;

	protected $restful = false;
	// protected $options;
	// PHP File Upload error message codes:
	// http://php.net/manual/en/features.file-upload.errors.php
	// protected $error_messages;
	// function __construct($options = null, $initialize = true, $error_messages = null) {}
	// protected function initialize() {}

	// protected function get_full_url() {}

	// protected function get_user_id() {}

	// protected function get_user_path() {}

	// protected function get_upload_path($file_name = null, $version = null) {}

	// protected function get_query_separator($url) {}
	function __construct($uploadHandlerArguments, $restful = false)
	{
		parent::__construct($uploadHandlerArguments['options'] , $uploadHandlerArguments['initialize'] , $uploadHandlerArguments['error_messages']);
		
		if($restful) $this->restful = $restful;
	}

	private function getRoute($file_name)
	{
		return $this->options['script_url'].'/'.rawurlencode($file_name);
	}

	protected function get_download_url($file_name, $version = null, $direct = false) {
		if($this->restful) return $this->getRoute($file_name);
		return parent::get_download_url($file_name, $version, $direct);
	}

	protected function set_additional_file_properties($file) {
		parent::set_additional_file_properties($file);
		if($this->restful) $file->delete_url = $this->getRoute($file->name);
	}

	// Fix for overflowing signed 32 bit integers,
	// works for sizes up to 2^32-1 bytes (4 GiB - 1):
	// protected function fix_integer_overflow($size) {}

	// protected function get_file_size($file_path, $clear_stat_cache = false) {}

	// protected function is_valid_file_object($file_name) {}

	// protected function get_file_object($file_name) {}

	// protected function get_file_objects($iteration_method = 'get_file_object') {}

	// protected function count_file_objects() {}

	// protected function create_scaled_image($file_name, $version, $options) {}

	// protected function get_error_message($error) {}

	// function get_config_bytes($val) {}

	// protected function validate($uploaded_file, $file, $error, $index) {}

	protected function upcount_name_callback($matches) {
		$index = isset($matches[1]) ? intval($matches[1]) + 1 : 1;
		$ext = isset($matches[2]) ? $matches[2] : '';
		return '__'.$index.$ext;
	}

	protected function upcount_name($name) {
		return preg_replace_callback(
			'/(?:(?:__([\d]+))?(\.[^.]+)?)?$/',
			array($this, 'upcount_name_callback'),
			$name,
			1
		);
	}


	/**
	* remove special utf8 characters
	* @param String $string the string to be sanitized
	* @return String
	*/
	private function sanitize($string){
		$strict = utf8_encode($string);
		$strict = strtolower($strict);
		$strict = iconv('UTF-8','ASCII//IGNORE',$strict);

		return $strict;
	}

	protected function get_unique_filename($name, $type = null, $index = null, $content_range = null) {
		//sanitize the basename of the file
		$path_parts = pathinfo($name);

		$extension = isset($path_parts['extension']) ? $path_parts['extension'] : '';
		$fileName = $path_parts['filename'];

		$fileName = $this->sanitize($fileName);

		$name = "{$fileName}.{$extension}";

		//upcount
		while(is_dir($this->get_upload_path($name))) {
			$name = $this->upcount_name($name);
		}
		// Keep an existing filename if this is part of a chunked upload:
		$uploaded_bytes = $this->fix_integer_overflow(intval($content_range[1]));
		while(is_file($this->get_upload_path($name))) {
			if ($uploaded_bytes === $this->get_file_size(
					$this->get_upload_path($name))) {
				break;
			}
			$name = $this->upcount_name($name);
		}

		return $name;
	}

	// protected function trim_file_name($name, $type = null, $index = null, $content_range = null) {}

	// protected function get_file_name($name, $type = null, $index = null, $content_range = null) {}

	// protected function handle_form_data($file, $index) {}

	// protected function orient_image($file_path) {}

	// protected function handle_image_file($file_path, $file) {}

	protected function is_upload_done($content_range)
	{
		return (is_null($content_range) || $content_range[3]-1==$content_range[2]);
	}

	protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null) {
		
		$file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error, $index, $content_range);
		
		if($this->is_upload_done($content_range))
			$event = Event::fire('filehandler.done',array($file, $error, $index, $content_range));
		else
			$event = Event::fire('filehandler.progress',array($file, $error, $index, $content_range));

		return $file;
	}

	// protected function readfile($file_path) {}

	// protected function body($str) {}

	// protected function header($str) {}

	// protected function get_server_var($id) {}

	// protected function generate_response($content, $print_response = true) {}

	// protected function get_version_param() {}

	// protected function get_file_name_param() {}

	// protected function get_file_type($file_path) {}

	// protected function download() {}

	// protected function send_content_type_header() {}

	// protected function send_access_control_headers() {}

	// public function head() {}

	// public function get($print_response = true) {}

	// public function post($print_response = true) {}

	public function delete($print_response = true)
	{
		$response = parent::delete($print_response);
		$event = Event::fire('filehandler.delete',array($response));
		return $response;
	}

	/**
	* get the path to the file or false if the file does not exist
	*/
	public function getFilePath($fileName)
	{
		$upload_dir = Config::get('filehandler::uploadHandler.upload_dir');
		$file = $upload_dir.$fileName;
		return File::exists($file) ? $file : false;
	}

}