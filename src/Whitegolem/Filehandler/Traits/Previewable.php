<?php namespace Whitegolem\Filehandler\Traits;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;

use Intervention\Image\Image;

trait Previewable {

	/**
	 * The image extensions available for the preview
	 *
	 * @var array
	 */
	protected $imageExtensions = array('png','jpg','jpeg','gif');

	/**
	* lifetime of the cached images in minutes
	*
	* @var int
	*/
	protected $chacheLifetime = 525600; //525600 = 60*24*365 = 1 year;

	/**
	 * Permit custom sizes
	 *
	 * @var boolean
	 */
	protected $customSizeAllowed = true;

	/**
	* provides a not found image on error 404
	* can be overridden unless it returns an array with the image data and the mime type
	* @return array 2-dimensional array with the image data and the mime type
	*/
	static protected function getFileNotFound()
	{
		$encodedData = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAFG0lEQVR4XtWUeWyUVRfGf/fdp9PpQltqaSm0GkggLNoABVHBJZHKUrbERENEmqA0klCMfsmH3z+iMUFJiELCakRQg/IHJi4hiBhSiERjQAPE1lI+9pYW6KgzQ2fm+p68Y4pFBQOFeJIn58y5z3nO03uno7TW3M4w+JeGsX8JLx58Hi2QWnq37AbqqimIeLw2csnHCKSW3q0yYNeNY3v/qrnQ8o4AqaUH2H1tQL03j/GR/Kz7iqpqoHm7AKmlJ2eA6ksDztACPhj0YD0cXgmOLZAa6cmZcPrKgPHZQublDawoCRdGoOtHsDyB1Egv1z8TjnD7woBbEmZdyYTHoXk9uB5kW4KgblpP+eQ6hCPcm23A/LKelwuGjSHkXIJkFEIWeYuGC6SGVBQ71Uw/nyNcwLxpBhZNpKAwh6WlY2vg1I7g2h2bbDctkDrondlJ2djHKMxRS2XmZhmwnryHdcWjH0HFjwJpsCwfJqbSAqmDHmmfc4Ti0Q8jM4B1owbUtqeYkJvvzSgaMQ7aG8FwfTg+bFJpBFJneq5wEK7MyCygbsSAMziXNWXV0+DCPjAdsCwwTVAmsQQCqaWXOXOES6k/I7PX+rdUf2fu84XUVZTnrx0y62k4uQOwghEFuBrCGgB+UZBQoAE0kISyGbR88j5NTacXPrqWDUCa3nGNN3L7Z7Gq/IFZcOEbMG3A7Lk3R5M3PQLAxe1RSKmeFSjo2E9J9RQundq4CngXiP0TA+aeepbnV1Z4Xr6C9s7gjVE9BmRfWmf22b0e04RklFCRQjT21B9bPmk1LwCp6zXg5To0DLp/KnR9G7ytMkATwAhy16dJyZAwM6YIQgnSED2MaFxsebMB+B/wy/V8Cc19i9laOn4CyuwA1Q0hB7Iyv/u2Ba4d9GwflmQbTEsQnIdsyHJlVjQQLdEEzGsZUKvnMCQSYkbRyCpInICww+tb4qgxbRAOFu8+kCTvwXZU1TnyJrez++skeGLO7sV3IH6CohFVZPuab/nagPo7A/bYMj4qGT8JUqfBhNhFxbKNP+NkKXAt8EwWrOhi2kQP3TJYsnwGz5JzYpe0z48GfMcGpUWLQZOnMK6MDwH7rwyozU9QnZVjDyu4exSkOiHsUvtSJ9MnhgANliWitJ7u5r/z8iFuSqb1jDyTDdk2tcuu5NtgOnC5g5yhd+JrD9+xgKmA+jMDzohidlbWzIRfj4Nj8dXeGLsOxNj2xgCSSR28r2WC1iS1AsxMhu52TePe+B/5whUYFsROItrluWwF3N4GjF3P0hAZUOh6d+RBOgGuw/zlbaz9TzFUhEUH4gbYDiiw7OA2JIPGvivs88/9kZ/I8A0bUglE29/h+buWAMaVBrwCl1crp9ZCoh0MU8Cx1gTPvHYWt/A7bBPUkO8R5coym1fe7oRcP2/qoLLUBm3QdCzei/8DOL/fginayA7ZBXhkwmxczKYzW4Zr3Tpf66O1AZpnan1yttZn52h9Ya6GgVq3z9X61Gz9xeaHdG6/So1drnPyK/Tx3TVan5h9Nb/Nr3+apfWRWh+BruyQXY3PsQkwFZB9sIHoyIZF0N0BqRSgQNEDoQlSGtIaLAWOAdE0ZBtwOQ1JDfpqPt1BPwgNpgl2AYdWrmHUSiIW4KEA67KPMOg0vaLHkNHzEQ1EMtnKAH0139a9pKR5GSukAO1ZgDofp/HQig336hRo+jaUwATZKaUCIkA5UAhY3JpIAueB/1tATAqgDVDcmtBAHIhxu+M38YHVTgpGi7EAAAAASUVORK5CYII=";
		return array(
			'image' => base64_decode($encodedData),
			'mime' => "image/png"
		);
	}

	protected function isImage($file)
	{
		$extension = File::extension($file);
		if(in_array(Str::lower($extension), $this->imageExtensions))
			return true;
		return false;
	}

	protected static function dataURI($img, $mime)
	{
		return "data:$mime;base64,".base64_encode($img);
	}

	/**
	 * check if the size provided for the preview image is customized
	 * a customized size is in this format:
	 *  100x100
	 *  100x_
	 *  _x100
	 *
	 * @return mixed an array with the custom width and height or the string with the original size provided
	 */
	private function checkCustomSize($size)
	{
		if(preg_match('/^([_\d]+)x([_\d]+)$/', $size, $matches))
		{
			$width = is_numeric($matches[1]) ? (int)$matches[1] : null;
			$height = is_numeric($matches[2]) ? (int)$matches[2] : null;
			return $size = array($width,$height);
		}
		return $size;
	}

	/**
	* resize an image using the Intervention/Image library
	* @param string $file the full path to the image file
	* @param mixed $size 2 dimensional array with width and height or a string with predefined size
	* @param boolean $dataUri wheter to return a dataURI image
	* @param boolean @ratio wheter to keep aspect ration when resizing
	* @param boolean $upsize wheter to enlarge an image when resizing
	* @return mixed can return a 2-dimensional array with image data and mime type or a dataURI scheme
	*/
	public function getPreview($file, $size, $dataURI = false, $ratio = true, $upsize = false)
	{
		
		if(!file_exists($file) || !$this->isImage($file)) return static::getFileNotFound();

		$sizeList = Config::get('filehandler::previewSize');

		$customSize = $this->checkCustomSize($size);

		if(is_array($customSize) && $this->customSizeAllowed)
		{
			$size = $customSize;
			list($width, $height) = $size;
		}else
		{
			if(is_array($size) && !$this->customSizeAllowed) $size = 'medium';
			list($width, $height) = isset($sizeList[$size]) ? $sizeList[$size] : $sizeList['medium'];
		}


		$img = Image::cache(function($image) use ($file, $width, $height, $ratio, $upsize) {
			return $image
				->make($file)
				->resize($width, $height, $ratio, $upsize)
			;
		},$this->chacheLifetime,false);
		
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $file);

		if($dataURI)
			return static::dataURI($img, $mime);

		return array(
			'image' => $img,
			'mime' => $mime
		);

	}

	/**
	 * print the image with the provided MIME type
	 */
	public function printPreview($image, $mime)
	{
		$response = Response::make($image, 200);
		$response->header('Content-Type', $mime);

		return $response;
	}

	public static function getPictureFullPath($path)
	{
		$fullPath = Config::get('filehandler::basePath').$path;
		if(File::exists($fullPath)) return $fullPath;
		return null;
	}
}