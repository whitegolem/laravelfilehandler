<?php namespace Whitegolem\Filehandler;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;

class FilehandlerServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('whitegolem/filehandler');
		include __DIR__.'/../../routes.php'; //to load a routes file for my package
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		$this->app['filehandler'] = $this->app->share(function($app)
		{
			//===============================
			//gestione file tramite jQuery File Upload
			// error_reporting(E_ALL | E_STRICT);

			$uploadHandlerArguments = array(
				'options' => \Config::get('filehandler::config.uploadHandler'),
				'initialize' => false, //con false posso gestire manualmente l'interfaccia rest
				'error_messages' => null
			);

			$restful = \Config::get('filehandler::config.restful');

			return new FileHandler($uploadHandlerArguments,$restful); 
		});

		/*
		|--------------------------------------------------------------------------
		| Events
		|--------------------------------------------------------------------------
		|
		| listen for deletion.
		| fire this event whene deleting a model which manages files
		|
		*/
		Event::listen('filehandler.dbdelete', function($file) {
			$_GET['file'] = $file;
			$upload_handler = $this->app->make('filehandler');
			$data = $upload_handler->delete(false); //delete(false) does not print directly
			return Response::json($data);
		});
		Event::listen('filehandler.delete', function($response) {
			$db_sync = \Config::get('filehandler::config.db_sync');
			if(!$db_sync) return;

			foreach($response as $file=>$success)
			{
				if($success)
				{
					$attachment = Attachment::where('name','=',$file)->delete();
				}
			}
			// return Response::json($attachment);
		});
		Event::listen('filehandler.done', function($file, $error, $index, $content_range) {
			$db_sync = \Config::get('filehandler::config.db_sync');
			if(!$db_sync) return;
			
			$attachment = Attachment::create(array(
				'name'=>$file->name
			));
		});
		Event::listen('filehandler.progress', function($file, $error, $index, $content_range) {});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('filehandler');
	}

}