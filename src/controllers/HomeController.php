<?php namespace Whitegolem\Filehandler;

use Illuminate\Routing\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller {

	public function __construct()
	{

	}

	private function applyParams($data)
	{
		//filter results 
		if($query = Input::get('q'))
		{
			$files = $data['files'];
			$data['files'] = array_filter($files,function($file) use($query){
				return preg_match('/'.$query.'/i', $file->name);
			});

		}
		return $data;
	}

	public function index()
	{
		$filehandler = App::make('filehandler');
		$download = Input::get('download', false);
		if($download)
		{
			return $filehandler->get(true);
		}
		$data = $filehandler->get(false); //get(false) does not print directly

		$data = $this->applyParams($data);

		if(Request::ajax())
		{
			return Response::json($data);
		}
		return View::make('filehandler::index', $data);
	}

	public function download($fileName)
	{
		//display the image inline if the "preview" parameter is provided
		if($inline = Input::get('preview', false))
		{
			$size = Input::get('s');
			$filehandler = App::make('filehandler');
			$file = $filehandler->getFilePath($fileName);
			$previewData = $filehandler->getPreview($file, $size);
			list($image, $mime) = array($previewData['image'], $previewData['mime']);
			return $filehandler->printPreview($image, $mime);
		}

		$_GET['file'] = $fileName;
		$_GET['download'] = 1;
		$uploadDir = Config::get('filehandler::uploadHandler.upload_dir');
		$file = $uploadDir.$fileName;

		$headers = array(
			// 'Content-Description' => 'File Transfer',
			// 'Content-Disposition' => 'attachment; filename="' . $fileName .'"'
		);

		return Response::download($file, $fileName, $headers);

		// return App::make('filehandler')->get(true);
	}
	/**
	* UPLOAD DEI FILE
	*/
	public function upload_file()
	{
		// Event::listen('filehandler.done',function($file, $error, $index, $content_range){
		// 	return true;
		// });

		$data = App::make('filehandler')->post(false); //post(false) does not print directly
		// dd($data);

		return Response::json($data);
	}
	/**
	* CANCELLA I FILE
	*/
	public function destroy_file($fileName = null)
	{
		if($fileName) $_GET['file'] = $fileName;
		$data = App::make('filehandler')->delete(false); //delete(false) does not print directly
		return Response::json($data);
	}

	/**
	* PROVE
	*/
	protected function response_chunked($filename) {
		$chunksize = 1*(1024*1024); // how many bytes per chunk
		
		$headers = array('Content-Type: application/octet-stream');

		$handle = fopen($filename, 'rb');
		if ($handle === false) {
			return false;
		}

		return Response::stream(function() use($handle,$chunksize){

			if(!feof($handle))
				return fread($handle, $chunksize);
			fclose($handle);
		},200,$headers);


	} 

	protected function readfile_chunked($filename,$retbytes=true) {
		$chunksize = 1*(1024*1024); // how many bytes per chunk
		$buffer = '';
		$cnt =0;
		// $handle = fopen($filename, 'rb');
		$handle = fopen($filename, 'rb');
		if ($handle === false) {
			return false;
		}
		while (!feof($handle)) {
			$buffer = fread($handle, $chunksize);
			echo $buffer;
			ob_flush();
			flush();
			if ($retbytes) {
				$cnt += strlen($buffer);
				echo $cnt;
			}
		}
		$status = fclose($handle);
		if ($retbytes && $status) {
			return $cnt; // return num. bytes delivered like readfile() does.
		}
		return $status;
	} 

}