<?php namespace Whitegolem\Filehandler;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

ini_set("memory_limit","640M");
ini_set("upload_max_filesize", "1000M");
ini_set("post_max_size", "1000M");

Route::get('files', array('as' => 'filehandler.files', 'uses' => 'Whitegolem\Filehandler\HomeController@index'));
Route::get('files/{fileName?}', array('as' => 'filehandler.file', 'uses' => 'Whitegolem\Filehandler\HomeController@download'));
Route::put('files', array('as' => 'filehandler.multipart_upload_file', 'uses' => 'Whitegolem\Filehandler\HomeController@upload_file'));
Route::post('files', array('as' => 'filehandler.upload_file', 'uses' => 'Whitegolem\Filehandler\HomeController@upload_file'));
Route::delete('files/{fileName?}', array('as' => 'filehandler.destroy_file', 'uses' => 'Whitegolem\Filehandler\HomeController@destroy_file'));

