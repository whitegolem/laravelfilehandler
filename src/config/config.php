<?php

return array(

	'version' => "1.0.0",
	// l'opzione seguente, impostata a true, influisce su $file->url e $file->delete_url
	'restful' => true,
	// il basepath per la preview delle immagini
	'basePath' => base_path('uploads/'),
	//list of default sizes used to resize the preview of the file
	'previewSize' => array(
		'x-small' => array(64,64),
		'small' => array(128,128),
		'medium' => array(512,512),
		'large' => array(1024,1024),
	),
	// sync uploads to database (table attachments)
	'db_sync' => true,
	/**
	* UploadHandler-specific options
	*/
	'uploadHandler' => array(
		'param_name' => 'files',
		// Directory where to upload files
		'upload_dir'=> base_path().'/uploads/',
		// 'upload_dir'=> app('path.public').'uploads/',
		// Url to retrieve uploaded files
		'upload_url' => URL::route('filehandler.files').'/',
		// 'upload_url' => URL::asset('uploads').'/',

		'script_url' => URL::route('filehandler.files'),
		// Enable to provide file downloads via GET requests to the PHP script:
		//     1. Set to 1 to download files via readfile method through PHP
		//     2. Set to 2 to send a X-Sendfile header for lighttpd/Apache
		//     3. Set to 3 to send a X-Accel-Redirect header for nginx
		// If set to 2 or 3, adjust the upload_url option to the base path of
		// the redirect parameter, e.g. '/files/'.
		'download_via_php' => 1,
		// Set the following option to false to enable resumable uploads:
		'discard_aborted_uploads' => true,
		'image_versions' => array(
			// Uncomment the following version to restrict the size of
			// uploaded images:
			/*
			'' => array(
				'max_width' => 1920,
				'max_height' => 1200,
				'jpeg_quality' => 95
			),
			*/
			// Uncomment the following to create medium sized images:
			'medium' => array(
				'max_width' => 800,
				'max_height' => 600,
				'jpeg_quality' => 80
			),
			'thumbnail' => array(
				// Uncomment the following to force the max
				// dimensions and e.g. create square thumbnails:
				//'crop' => true,
				'max_width' => 80,
				'max_height' => 80
			)
		)
	),
);