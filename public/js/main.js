
	function loadFilehandlerScripts(basePath) {
		var basePath = basePath || '/',
			url,
			sources = [
				'js/jQuery-File-Upload/js/vendor/jquery.ui.widget.js',
				'js/jQuery-File-Upload/js/jquery.iframe-transport.js',
				'js/jQuery-File-Upload/js/jquery.fileupload.js',
				'js/UploadManager.js'
			], i, source;

		for(i in sources) {
			url = [basePath,sources[i]].join('/');

			document.write('<' + 'script src="' + url + '"' +
			' type="text/javascript"><' + '/script>');
		}
	}

