;(function($, window, document, undefined){
/**
* @AUTHOR Francesco Delacqua
*
* gestione uploads
*
* il pulsante deve avere i seguenti attributi:
* - data-upload-url
*
*/

var pluginName = "uploadManager",
	defaults = {
		uploadUrl: '/files',
		deleteUrl: '/files?file=',
		uploadButtonTemplate: [
			'<input type="file" name="files[]">',
		].join(''),
		progressBarTemplate: [
			'<div class="progress progress-striped active" style="margin:0;">',
				'<div class="bar" style="width: 0%;"></div>',
			'</div>'
		].join(''),
		startUploadButtonTemplate: ['<span class="btn btn-info">upload</span>'].join(''), 
		abortUploadButtonTemplate: ['<span class="btn btn-warning">pausa</span>'].join(''),
		deleteButtonTemplate: ['<span class="btn btn-danger">elimina</span>'].join(''),
		multiFileStringSeparator: ':', //usato in caso di upload multipli
		multiple: false,
		autostart: false,
		uploadAddCallback: function(e, data){},
		uploadProgressCallback: function(e, data){},
		doneTargetSelector: null,
		uploadDoneCallback: function(e, data){
			var self = this;

			if(data.context){
				data.context.empty();
			}
			$.each(data.result.files, function (index, file) {
				// $(self.options.doneTargetSelector).val(file.name);
				$link = $("<a/>")
							.attr('href',file.url)
							.text(file.name)
							.appendTo(data.context);
				$deleteButton = $(self.options.deleteButtonTemplate)
							.on('click', function(e){
								self.deleteFile(file.name).done(function(result){
									data.context.remove();
								});
								e.preventDefault();
							})
							.appendTo(data.context);
			});
		}
	};


function Plugin(element, options) {
	this.element = element;
	this.options = $.extend( {}, defaults, options);
	this._defaults = defaults;

	this.init();
}

Plugin.prototype.init = function() {
	try {
		this.uploadUrl = $(this.element).data('uploadUrl');
		this.uploadButton = $(this.options.uploadButtonTemplate)
								.appendTo(this.element);
		if(this.options.multiple) this.uploadButton.attr('multiple','multiple');
		this.filesTable = $('<table/>').addClass('table table-striped').appendTo(this.element);

		this.initjQueryFileUpload();
		this.bindEvents();
	}catch(e) {
		alert("errore durante l'inizializzazione di uploadManager");
	}
};

Plugin.prototype.bindEvents = function() {
	var self = this;

	$(self.element).on('click',function(e){
		// var modelID = $(this).data('model-id'),
		// 	url = $(self.element).data('delete-url');
		
		// if(url && modelID) {
		// 	self.$deleteModal.modal();
		// }
		// e.preventDefault();
	});

};

Plugin.prototype.deleteFile = function(fileName) {
	var self = this;

	return $.ajax({
		type: 'DELETE',
		url: [self.options.deleteUrl,fileName].join('/'),
		data: {file: fileName} //this is ignored. probably for RFC compliance because DELETE has no body
	});
};


$.fn[pluginName] = function(options){
	return this.each(function(){
		if ( !$.data(this, "plugin_" + pluginName )) {
			$.data( this, "plugin_" + pluginName,
			new Plugin( this, options )); //salvo un riferimento alla plugin nell'elemento
		}
	});
};

Plugin.prototype.addFileRow = function(data) {
	var self = this,
		fileName = data.files[0]['name'],
		$row = $('<tr/>').addClass('fade in'),
		$nameColumn = $('<td/>').addClass('span6').appendTo($row),
		$progressColumn = $('<td/>').addClass('span4').appendTo($row),
		$actionsColumn = $('<td/>').addClass('span2').appendTo($row),
		$name, $progressBar, $startButton, $abortButton,
		jqXHR;

	$name = $('<span/>').text(fileName).appendTo($nameColumn);
	$progressBar = $(self.options.progressBarTemplate).appendTo($progressColumn);
	$startButton = $(self.options.startUploadButtonTemplate).appendTo($actionsColumn);
	$abortButton = $(self.options.abortUploadButtonTemplate).appendTo($actionsColumn);
	// $deleteButton = $(self.options.deleteButtonTemplate).appendTo($actionsColumn);

	$abortButton.hide(); //nascosto inizialmente

	$startButton.on('click', function(e){
		data.context = $row;
		jqXHR = data.submit();
		$(this).hide();
		$abortButton.show();
		e.preventDefault();
	});

	$abortButton.on('click', function(e){
		jqXHR.abort();
		self.deleteFile(fileName).done(function(result){
			if(result.success) {
				data.context.remove();
				data.context = undefined;
			}
		});
		$(this).hide();
		$startButton.show();
		e.preventDefault();
	});

	$row.appendTo(self.filesTable);

	return $row;

};

Plugin.prototype.initjQueryFileUpload = function() {

		var self = this,
			$row;

		$(self.uploadButton).fileupload({
			url: self.options.uploadUrl,
			dataType: 'json',
			// Uncomment the following to send cross-domain cookies:
			//xhrFields: {withCredentials: true},
			maxChunkSize: 1000000,//10000000
			done: function(e, data){
				//uso una closure e apply per mantenere lo scope
				self.options.uploadDoneCallback.apply(self, arguments);
			},
			add: function (e, data) {
				var $row = self.addFileRow(data);
			},
			progress: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				data.context.find('.progress .bar').css({'width': progress + '%'});
			},

		});
};

}( jQuery, window, document ));